# login_in = "Johnny"
# login_passw = "docusign"


class Mouton:
    nom: str

    def __init__(self, nom):
        self.nom = nom

    def crier(self):
        return "Bhéééééééé"

    def __str__(self):
        return f"Je m'appelle {self.nom} et je suis un mouton {self.crier()}"
    

class Chien:
    nom: str

    def __init__(self, nom):
        self.nom = nom

    def crier(self):
        return "whoof whoof"

    def __str__(self):
        return f"Je m'appelle {self.nom} et je suis un chien {self.crier()}"



if __name__ == '__main__':
    # tests
    shaun = Mouton('Shaun')
    print(shaun.crier())
    # → Bhéééééééé
    print(shaun)
    # → Je m'appelle Shaun et je suis un mouton.

    # créer un autre mouton qui s'appelle kebab
    kebab = Mouton("kebab")
    # faire un print de kebab
    print(kebab)
    # → Je m'appelle kebab et je suis un mouton.
    # créer un troupeau de 138 moutons (comme ils sont trop nombreux ils ont juste un numéro)
    troupeau = []
    for i in range(138):
        troupeau.append(Mouton(i))
    # ajouter Shaun et Kebab au troupeau
    troupeau.append(shaun)
    troupeau.append(kebab)
    # attaque de loup !
    # faire crier tout le troupeau
    for mouton in troupeau:
        print(mouton.crier())

    kiki = Chien('kiki')
    print(kiki.crier())
    
    print(kiki)
    # créer kiki le chien.
    # faire un print de kiki
    # → Je m'appelle kiki et je suis un chien wouf wouf
    # faire crier kiki
    # → wouf wouf