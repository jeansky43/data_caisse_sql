DROP DATABASE IF EXISTS data_caisse;
CREATE DATABASE data_caisse;
USE data_caisse;
GRANT ALL PRIVILEGES ON data_caisse.* TO 'John'@'localhost';
CREATE TABLE `Product` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL,
    `description` VARCHAR(255) NOT NULL,
    `images` TEXT NOT NULL,
    `price` DECIMAL(8, 2) NOT NULL,
    `available` TINYINT(1) NOT NULL
);

CREATE TABLE `Adress` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `street` VARCHAR(255) NOT NULL,
    `city` VARCHAR(255) NOT NULL,
    `zipcode` INT NOT NULL,
    `state` VARCHAR(255) NOT NULL,
    `country` VARCHAR(255) NOT NULL
);

CREATE TABLE `Client` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `name` VARCHAR(255) NOT NULL,
    `email` TEXT NOT NULL,
    `phone` VARCHAR(255),
    `default_address` INT UNSIGNED,
    FOREIGN KEY (`default_address`) REFERENCES `Adress`(`id`)
);

CREATE TABLE `Order` (
    `id` INT UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY,
    `id_client` INT UNSIGNED,
    `delivery_address` INT UNSIGNED,
    `billing_address` INT UNSIGNED,
    `payment_method` ENUM('Credit Card', 'Bank check on delivery', 'Cash on delivery'),
    `order_state` ENUM('Cart', 'Validated', 'Sent', 'Delivered'),
    FOREIGN KEY (`id_client`) REFERENCES `Client`(`id`),
    FOREIGN KEY (`delivery_address`) REFERENCES `Adress`(`id`),     FOREIGN KEY (`billing_address`) REFERENCES `Adress`(`id`)
);


CREATE TABLE `Client_adress` (
    `id_client` INT UNSIGNED,
    `id_address` INT UNSIGNED,
    FOREIGN KEY (`id_address`) REFERENCES `Adress`(`id`),
    FOREIGN KEY (`id_client`) REFERENCES `Client`(`id`)
);


CREATE TABLE `Order_line` (
    `product_id` INT UNSIGNED,
    `order_id` INT UNSIGNED,
    `qt` INT NOT NULL,
    FOREIGN KEY (`product_id`) REFERENCES `Product`(`id`),
    FOREIGN KEY (`order_id`) REFERENCES `Order`(`id`)
);


INSERT INTO Product (name, description, images, price, available)
VALUES 
("Truc", "Lorem Ipsum", "https://picsum.photos/200", 200, 1),
('Bidule', 'Lorem merol', 'https://picsum.photos/100', 500, 1);

INSERT INTO `Order` (order_state, payment_method) 
VALUES ('Cart', 'Credit Card');

INSERT INTO Order_line (order_id, product_id, qt) 
VALUES
(1, 1, 3),
(1, 1, 3),
(1, 1, 3),
(1, 2, 2);



INSERT INTO Client (name, email, default_address) 
VALUES
('Bob', 'bob@gmail.com', @last_adres_id );
SELECT LAST_INSERT_ID();

SET @last_client_id = LAST_INSERT_ID();

UPDATE Client 
SET phone = '0123456789' 
WHERE name = 'Bob' AND email = 'bob@gmail.com';

UPDATE `Order`
SET id_client = 1
WHERE id = Order.id;


INSERT INTO Adress (street, city, zipcode, state, country)
VALUES 
('12 street St', 'Schenectady', 12345, 'New York', 'US');
SELECT LAST_INSERT_ID();

SET @last_adres_id = LAST_INSERT_ID();

UPDATE `Order`
SET billing_address = 1
WHERE id = 1;
SELECT * FROM `Order`;

INSERT INTO Client_adress (id_client, id_address) 
VALUES 
(@last_adres_id, @last_client_id);

INSERT INTO Adress (street, city, zipcode, state, country)
VALUES 
('12 boulevard de Strasbourg', 'Toulouse', 31000, 'OC', 'France');
SELECT LAST_INSERT_ID();
SET @last_adres_id2 = LAST_INSERT_ID();

SET @last_client_id = 1;

INSERT INTO Client_adress (id_client, id_address) 
VALUES 
(@last_client_id, @last_adres_id2);

UPDATE `Order`
SET delivery_address = 2
WHERE id = 1;

UPDATE `Order`
SET order_state = 'Validated'
WHERE order_state = 'Cart';

UPDATE `Order`
SET order_state = 'Sent'
WHERE order_state = 'Validated';

SELECT a.*
FROM Client as c
JOIN Client_adress ca ON c.id = ca.id_client
JOIN Adress a ON ca.id_address = a.id
WHERE c.name = 'Bob';

SELECT o.*
FROM Client c
JOIN `Order` o ON c.id = o.id_client
JOIN Order_line ol ON o.id = ol.order_id 
JOIN Product p ON p.id = ol.product_id
WHERE c.name = 'Bob';



SELECT *
FROM Client c
JOIN `Order` o ON c.id = o.id_client
JOIN Order_line ol ON o.id = ol.order_id 
JOIN Product p ON p.id = ol.product_id
WHERE c.name = 'Bob';
















