from typing import Union
from pydantic import BaseModel

class Product(BaseModel):
    id: int | None = 1
    name: str | None = None
    description: str | None = None
    images: str | None = None
    price: float | None = None
    available: bool | None = None

class Adress(BaseModel):
    id: int | None = None
    street: str | None = None
    city: str | None = None
    zipcode: int | None = None
    state: str | None = None
    country: str | None = None

class Client(BaseModel):
    id: int | None = None
    name: str | None = None
    email: str | None = None
    phone:str | None = None
    default_address: int | None = None

class Order(BaseModel):
    id: int | None = None
    id_client: int | None = None
    delivery_address: int | None = None
    billing_address:int | None = None
    payment_method: str | None = None
    order_state: str | None = None

class Client_adress(BaseModel):
    id_client: int | None = None
    id_address: int | None = None

class Order_line(BaseModel):
    product_id: int | None = None
    order_id: int | None = None
    qt: int | None = None

