from typing import Union
from fastapi import FastAPI
import mysql.connector
from model_dto import Product


conn = mysql.connector.connect(
    host="localhost",
    user="John",
    password="docu",
    database="data_caisse"
)

mycursor = conn.cursor()
app = FastAPI()

# @app.get('/`Order`')
# def get_order():
#     r = mycursor.execute("SELECT * from `Order`")
#     return mycursor.fetchall()

# @app.get('/Product')
# def get_product():
#     r = mycursor.execute("SELECT * from Product")
#     return mycursor.fetchall()

# @app.get('/Client')
# def get_client():
#     r = mycursor.execute("SELECT * from Client")
#     return mycursor.fetchall()

# @app.get('/Adress')
# def get_adress():
#     r = mycursor.execute("SELECT * from Adress")
#     return mycursor.fetchall()

# @app.get('/Client_adress')
# def get_cliad():
#     r = mycursor.execute("SELECT * from Client_adress")
#     return mycursor.fetchall()

# @app.get('/Order_line')
# def get_ordline():
#     r = mycursor.execute("SELECT * from Order_line")
#     return mycursor.fetchall()

# Liste des produits en JSON
@app.get('/Product')
def get_product():
    r = mycursor.execute("SELECT * from Product")
    return mycursor.fetchall()

# Un produit en JSON
@app.get('/Product{1}')
def get_product():
    r = mycursor.execute("SELECT Product.name, Product.description, Product.images, Product.price, Product.available from Product WHERE id = 1")
    return mycursor.fetchall()

# Le même produit en JSON avec son id
@app.post("/Product")
async def create_product(produit: Product):
    try:
        mycursor = conn.cursor()        
        sql = "INSERT INTO Product (name, description, images, price, available) VALUES (%s, %s, %s, %s, %s)"
        val = (produit.name, produit.description, produit.images, produit.price, produit.available)
        mycursor.execute(sql, val)

        conn.commit()
        mycursor.close()        
        return produit
    except mysql.connector.Error as e:
        return {"error": f"Une erreur s'est produite : {e}"}

# Mettre à jour le produit
@app.post("/Product/{product_id}")
async def update_product(product_id: int):
    try:        
        mycursor = conn.cursor()        
        sql = "UPDATE Product SET price = 300 WHERE id = %s"
        val = (product_id,)  

        mycursor.execute(sql, val)
        conn.commit()

        mycursor.close()

        return {"message": f"Prix du produit ayant l'ID={product_id} mis à jour avec succès"}

    except mysql.connector.Error as e:
        return {"error": f"Une erreur s'est produite : {e}"}
    
# Supprimer le produit
@app.post("/Product/{product_id}")
async def delete_product(product_id: int):
    try:
        
        mycursor = conn.cursor()        
        sql = "DELETE FROM Product WHERE id = %s"        
        mycursor.execute(sql, (product_id,))        
        conn.commit()        
        mycursor.close()

        return {"message": f"Produit avec l'ID={product_id} supprimé avec succès"}

    except mysql.connector.Error as e:
        return {"error": f"Une erreur s'est produite : {e}"}