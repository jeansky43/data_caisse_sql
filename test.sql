USE data_caisse;
-- DELETE FROM Client;


-- ### Créer un produit "Truc" et "Bidule"
-- INSERT INTO Product (name, description, images, price, available)
-- VALUES 
-- ("Truc", "Lorem Ipsum", "https://picsum.photos/200", 200, 1),
-- ('Bidule', 'Lorem merol', 'https://picsum.photos/100', 500, 1);

-- ### Récupérer la liste des produits et vérifier le prix de Truc et de Bidule.
-- SELECT * FROM Product;
-- SELECT name, price FROM Product;

-- ### Récupérer Truc à partir de son id et vérifier sa description et son image.
-- SELECT name, description, images
-- FROM Product
-- WHERE id = 1;



-- ### Créer une nouvelle commande
-- INSERT INTO `Order` VALUES ();
-- SELECT * FROM `Order`;

-- ### Ajouter 3 "Truc" et 1 Bidules à la commande
-- INSERT INTO Order_line (order_id, product_id, qt) VALUES
-- (1, 1, 3),
-- (1, 1, 3),
-- (1, 1, 3),
-- (1, 2, 2);
-- SELECT * from Order_line;

-- ### Créer le client “Bob” avec comme mail “bob@gmail.com”.
-- INSERT INTO Client (name, email) 
-- VALUES
-- ('Bob', 'bob@gmail.com');

-- ### Mettre à jour Bob pour ajouter son numéro de téléphone “0123456789”.
-- UPDATE Client SET phone = '0123456789' WHERE name = 'Bob' AND email = 'bob@gmail.com';
-- SELECT * FROM Client;

-- ### Essayer de passer la commande dans l’état suivant (vérifier qu’il y a une erreur).
-- UPDATE `Order`
-- SET order_state = 'Validated'
-- WHERE order_state = 'Cart';
-- SELECT * FROM `Order`;

-- ### Associer Bob à la commande
-- UPDATE `Order`
-- SET id_client = 1
-- WHERE id = Order.id;
-- SELECT * from `Order`;

-- ### Créer l’adresse “12 street St, 12345 Schenectady, New York, US”
-- INSERT INTO Adress (street, city, zipcode, state, country)
-- VALUES 
-- ('12 street St', 'Schenectady', 12345, 'New York', 'US');
-- SELECT LAST_INSERT_ID();
-- SELECT * FROM Adress;

-- ### Associer cette adresse à Bob.
-- ### Insérer l'adresse et Récupérer l'identifiant de l'adresse insérée
-- INSERT INTO Adress (street, city, zipcode, state, country)
-- VALUES 
-- ('12 street St', 'Schenectady', 12345, 'New York', 'US');
-- SELECT LAST_INSERT_ID();
-- SET @last_adres_id = LAST_INSERT_ID();

-- ### Insérer Bob dans la table Client en associant l'adresse à sa colonne default_address et -- Récupérer l'identifiant du client Bob
-- INSERT INTO Client (name, email, default_address) 
-- VALUES
-- ('Bob', 'bob@gmail.com', @last_adres_id );
-- SELECT LAST_INSERT_ID();
-- SET @last_client_id = LAST_INSERT_ID();

-- ### Insérer une entrée dans la table Client_adress pour connecter Bob à cette adresse
-- INSERT INTO Client_adress (id_client, id_address) 
-- VALUES 
-- (@last_adres_id, @last_client_id);
-- SELECT * FROM Client_adress;

-- ### Essayer d’associer cette adresse comme facturation à la commande.
-- UPDATE `Order`
-- SET billing_address = 1
-- WHERE id = 1;
-- SELECT * FROM `Order`;

-- ### Créer l’adresse “12 boulevard de Strasbourg, 31000, Toulouse, OC, France”
-- INSERT INTO Adress (street, city, zipcode, state, country)
-- VALUES 
-- ('12 boulevard de Strasbourg', 'Toulouse', 31000, 'OC', 'France');
-- SELECT LAST_INSERT_ID();
-- SET @last_adres_id2 = LAST_INSERT_ID();

-- ### Associer cette adresse à Bob.
-- Récupérer l'identifiant du client Bob (vous pouvez le remplacer par l'identifiant de Bob si vous le connaissez déjà)
-- SET @last_client_id = (SELECT id FROM Client WHERE name = 'Bob' LIMIT 1);
-- SET @last_client_id = 1

-- Insérer une entrée dans la table Client_adress pour connecter Bob à cette nouvelle adresse
-- INSERT INTO Client_adress (id_client, id_address)
-- VALUES (@last_client_id, @last_adres_id2);

-- ### Associer cette adresse comme adresse de livraison
-- UPDATE `Order`
-- SET delivery_address = 2
-- WHERE id = 1;
-- SELECT * FROM `Order`;

-- ### Associer le moyen de paiement “Card” à la commande.
-- INSERT INTO `Order` (order_state, payment_method) 
-- VALUES ('Cart', 'Credit Card');

-- ### Passer la commande dans l’état suivant..
-- UPDATE `Order`
-- SET order_state = 'Sent'
-- WHERE order_state = 'Validated';
-- SELECT * FROM `Order`;

-- ### Passer la commande dans l’état suivant.
-- UPDATE `Order`
-- SET order_state = 'Delivered'
-- WHERE order_state = 'Sent';
-- SELECT * FROM `Order`;

-- ### Vérifier que la commande est dans l’état “sent”.
SELECT o.*
FROM Client c
JOIN `Order` o ON c.id = o.id_client
JOIN Order_line ol ON o.id = ol.order_id 
JOIN Product p ON p.id = ol.product_id
WHERE c.name = 'Bob';
-- ### Récupérer la liste des adresses de Bob.
SELECT a.*
FROM Client as c
JOIN Client_adress ca ON c.id = ca.id_client
JOIN Adress a ON ca.id_address = a.id
WHERE c.name = 'Bob';

-- ### Récupérer la liste des commandes de Bob.
SELECT p.*
FROM Client c
JOIN Order o ON c.id = o.id_client
JOIN Order_line ol ON o.id = ol.order_id 
JOIN Product p ON p.id = ol.product_id
WHERE c.name = 'Bob';




    

